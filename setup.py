from setuptools import find_packages, setup

setup(
    name="example_package",
    version="0.0.2",
    author="Nikita Vechtomov",
    description="A small example package",
    url="https://gitlab.com/NikitaVechtomov/packaging_tutorial",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(),
    python_requires=">=3.6",
    include_package_data=True,
)
